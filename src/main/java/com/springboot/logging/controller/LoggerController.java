package com.springboot.logging.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.springboot.logging.model.ExceptionDto;
import com.springboot.logging.model.TransactionDto;
import com.springboot.logging.repository.ExceptionRepository;
import com.springboot.logging.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.QueryParam;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@RestController
public class LoggerController {

    @Autowired
    ExceptionRepository exceptionRepository;
    @Autowired
    TransactionRepository transactionRepository;

    @HystrixCommand(commandProperties= {@HystrixProperty(name="execution.isolation.thread.timeoutInMilliseconds", value="12000")})
    @PostMapping(value = "/logException")
    public String saveException (@QueryParam("message") String message,
                                 @QueryParam("service") String service){
        randomlyRunLong();

        ExceptionDto exceptionDto = new ExceptionDto(message,service);
        exceptionRepository.save(exceptionDto);
        return "Exception saved correctly";
    }

    @HystrixCommand(commandProperties= {@HystrixProperty(name="execution.isolation.thread.timeoutInMilliseconds", value="12000")})
    @PostMapping(value = "/logTransaction")
    public String saveTransaction (@QueryParam("message") String message,
                                   @QueryParam("status") String status,
                                   @QueryParam("service") String service){
        randomlyRunLong();

        TransactionDto transactionDto = new TransactionDto(message,status,service);
        transactionRepository.save(transactionDto);
        return "Transaction saved correctly";
    }

    @GetMapping(value = "/getExceptionLogs")
    public List<ExceptionDto> getExceptionLogs (){
        return exceptionRepository.findAll();
    }

    @GetMapping(value = "/getTransactionLogs")
    public List<TransactionDto> getTransactionLogs(){
        return transactionRepository.findAll();
    }

    private void randomlyRunLong(){
        Random rand = new Random();
        int randomNum = rand.nextInt((3 - 1) + 1) + 1;
        if (randomNum==3) sleep();
    }

    private void sleep(){
        try {
            Thread.sleep(11000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private String buildFallback(){
        return "Information was not saved do to comunication problems";
    }

}

