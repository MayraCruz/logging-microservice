package com.springboot.logging.model;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@Document (value = "TransactionLog")
public class TransactionDto {
    private String date;
    private String hour;
    private String message;
    private String status;
    private String service;

    public TransactionDto (String message, String status, String service){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
        Date rn = new Date();

        this.date = dateFormat.format(rn);
        this.hour = dateFormat.format(rn);
        this.message = message;
        this.status = status;
        this.service = service;
    }
}
