package com.springboot.logging.model;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@Document (value = "ExceptionLog")
public class ExceptionDto {
    private String date;
    private String hour;
    private String message;
    private String service;

    public ExceptionDto (String message, String service){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
        Date rn = new Date();

        this.date = dateFormat.format(rn);
        this.hour = hourFormat.format(rn);
        this.message = message;
        this.service = service;
    }
}
