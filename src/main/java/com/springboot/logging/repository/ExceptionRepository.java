package com.springboot.logging.repository;

import com.springboot.logging.model.ExceptionDto;
import com.springboot.logging.model.ExceptionDto;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ExceptionRepository extends MongoRepository <ExceptionDto, String> {
}
