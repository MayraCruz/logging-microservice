package com.springboot.logging.repository;

import com.springboot.logging.model.TransactionDto;
import com.springboot.logging.model.TransactionDto;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TransactionRepository extends MongoRepository <TransactionDto, String> {
}
