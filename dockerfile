FROM openjdk:8
COPY ["target/logging-app.jar", "/logging-app.jar"]

EXPOSE 8080
ENTRYPOINT ["java","-jar","logging-app.jar"]